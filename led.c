/*Toggle LED (Edge Triggering)*/

# define SYSCTL_RCGCGPIO_R (*(( volatile unsigned long *)0x400FE608)) // enable the clock
# define GPIO_PORTF_DATA_RD (*(( volatile unsigned long *)0x40025040)) //configure the direction register of port F
# define GPIO_PORTF_DATA_WR (*(( volatile unsigned long *)0x40025020))
# define GPIO_PORTF_DIR_R (*(( volatile unsigned long *)0x40025400))
# define GPIO_PORTF_DEN_R (*(( volatile unsigned long *)0x4002551C))
# define GPIO_PORTF_PUR_R (*(( volatile unsigned long *)0x40025510 )) // configure the PUR (Pull up resistor) register

# define SYSCTL_RCGC2_GPIOF 0x0020
# define GPIO_PORTF_PIN3_EN 0x08
# define GPIO_PORTF_PIN4_EN 0x10
# define SYSTEM_CLOC_FREQUENCY  16000000 //frequency of the system

/*DELAY_DEBOUNCE is a variable greater than the minimum time for which the switch can be pressed*/

#define DELAY_DEBOUNCE	SYSTEM_CLOC_FREQUENCY/1000
void Delay(unsigned long counter)
{
	unsigned long i = 0;
	for(i = 0; i<counter; i++);
}


int main ()
{
	static char flag = 0;
	SYSCTL_RCGCGPIO_R |= SYSCTL_RCGC2_GPIOF; // clock initialization of port F
	GPIO_PORTF_DEN_R |= GPIO_PORTF_PIN3_EN +GPIO_PORTF_PIN4_EN; //enable both the pins 3 and pin 4 of port F
	GPIO_PORTF_DIR_R |= GPIO_PORTF_PIN3_EN; //set the direction of the pins
	GPIO_PORTF_DIR_R &= (~GPIO_PORTF_PIN4_EN); // set the direction of the pins
	GPIO_PORTF_PUR_R |= GPIO_PORTF_PIN4_EN; //enable the pull up resistor register for pin 4 i.e. the pin with switch

/* If the switch is not pressed then de-bouncing time of switch is added, and again the state of the switch is checked*/
while(1) // loop will continue to be executed forever
{
	if(GPIO_PORTF_DATA_RD == 0) //check the current state of the input switch
	{
		Delay(DELAY_DEBOUNCE);
		if(( flag == 0) && (GPIO_PORTF_DATA_RD == 0))
		{
			GPIO_PORTF_DATA_WR ^= GPIO_PORTF_PIN3_EN; //^ is to toggle the previous state of the pin
			flag = 1; // LED will be toggled at the end of the if block the flag is updated to 1
		}
	}
	else
	{
		flag = 0; //else block will be executed and flag will be set to zero
	}
}

}
