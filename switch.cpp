/*switch 1 and switch 2 turn on different LEDs*/

#include <lm4f120h5qr.h>
int main()
{
	SYSCTL->RCGCGPIO = 0x20;// 0b100000 (Enable PORTF for GPIO)
	GPIOF->LOCK = 0x4C4F434B;// unlock data defined in datasheet, any other value will lock it
	GPIOF->CR = 0xff;// 0b111111 enable ability to writwe to PUR for PFO
	GPIOF->DIR = 0x0E;// 0b01110, make PF0 and PF4 inputs, make PF1, PF2, and PF3 outputs
	GPIOF->PUR = 0x11;// 0b10001, enable pull-up resistors on PF0 and PF4
	GPIOF->DEN = 0x1F;// 0b00011111, enable pins PF0 to PF4
	while(1){
		switch(GPIOF->DATA & 0x11){
			case 0x00 : //both switches pressed
				GPIOF->DATA = (1<<1); // 1<<1 == 0b10, turn on red LED
				break;
			case 0x01 : 
				GPIOF->DATA = (1<<2);// 1<<2 == 0b100 turn on blue LED
				break;
			case 0x10 : 
				GPIOF->DATA = (1<<3);// 1<<3 == 0b1000 turn on green LED
				break;
			default : 
				GPIOF->DATA &= ~((1<<1) | (1<<2) | (1<<3)); // GPIOF->DATA == GPIOF->DATA & 0b11110001
				break;
		}
	}
	return 0;
}