/*LED blinking using UART 

If a received character on the RX5 pin of UART5 module is ‘R’or 'G' or `B`, red, green, blue LED will turn on respectively 
and if the received message is ‘F’, currently on LED will turn off. For any other character, LED will retain its current state. */

#include "TM4C123.h"
#include <stdint.h>
#include <stdlib.h>

void Delay(unsigned long counter);
//char UART5_Receiver(void);
void UART5_Transmitter(unsigned char data);
void printstring(char *str);

void UART5_Handler( void ){
	unsigned char rx_data = 0;
	UART5->ICR &= ~(0x010); // Clear receive interrupt
	rx_data = UART5->DR ; // get the received data byte
	if(rx_data == 'R')
		GPIOF->DATA  = 0x02;
	else if(rx_data == 'B')
		GPIOF->DATA = 0x04;
	else if(rx_data == 'G')
		GPIOF->DATA = 0x08;
	else if(rx_data == 'F')
		GPIOF->DATA  = 0x00;
	UART5_Transmitter(rx_data); // send data that is received
}

int main(void){
	SYSCTL->RCGCUART |= 0x20;  /* enable clock to UART5 */
	SYSCTL->RCGCGPIO |= 0x10;  /* enable clock to PORTE for PE4/Rx and RE5/Tx */
	Delay(1);
	 /* UART0 initialization */
	UART5->CTL = 0;         /* UART5 module disbable */
	UART5->IBRD = 104;      /* for 9600 baud rate, integer = 104 */
	UART5->FBRD = 11;       /* for 9600 baud rate, fractional = 11*/
	UART5->CC = 0;          /*select system clock*/
	UART5->LCRH = 0x60;     /* data lenght 8-bit, not parity bit, no FIFO */
	UART5->CTL = 0x301;     /* Enable UART5 module, Rx and Tx */

	/* UART5 TX5 and RX5 use PE4 and PE5. Configure them digital and enable alternate function */
	GPIOE->DEN = 0x30;      /* set PE4 and PE5 as digital */
	GPIOE->AFSEL = 0x30;    /* Use PE4,PE5 alternate function */
	GPIOE->AMSEL = 0;    /* Turn off analg function*/
	GPIOE->PCTL = 0x00110000;     /* configure PE4 and PE5 for UART */
	  
	// enable interrupt 
	UART5->ICR &= ~(0x0780);; // Clear receive interrupt
	UART5->IM  = 0x0010;
	NVIC->ISER[1] |= 0x20000000; /* enable IRQ61 for UART0 */
	 
	SYSCTL->RCGCGPIO |= 0x20; // turn on bus clock for GPIOF
	GPIOF->DIR       |= 0x02; //set GREEN pin as a digital output pin
	GPIOF->DEN       |= 0x02;  // Enable PF3 pin as a digital pin
	Delay(1); 
		
	printstring("Enter the Command");
	Delay(10); 
	while(1){
	}
}


void UART5_Transmitter(unsigned char data){
	UART5_Handler();
	while((UART5->FR & (1<<5)) != 0); /* wait until Tx buffer not full */
	UART5->DR = data;                  /* before giving it another byte */
}

void printstring(char *str){
	while(*str){
		UART5_Transmitter(*(str++));
	}
}

void Delay(unsigned long counter){
	unsigned long i = 0;
	for(i=0; i< counter; i++);
}

